# Mayan EDMS manual project

Project repository for the Mayan EDMS manual written in AsciiDoc.

![Screenshot](https://gitlab.com/mayan-edms/manual/raw/master/screenshot.png)
